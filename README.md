# Filling-Conversation-Ellipsis

## Dialog Act Prediction

### Dataset preparation

Example Input format:

```
system utterance : previous user utterance : system dialog act & current user utterance ## dialog act 1 of current user utterance;dialog act 2 of current user utterance
```

EMPTY means that there is no previous user utterance and the current user utterance is the first utterance responding to the system

For example the dialog below with user current utterance's dialog act annotated as pos_answer can be formatted as: "do you want to talk about music  :  yes  : yes_no_question  & let's talk about music ##  command;"

```
System: do you want to talk about music(dialog act: yes_no_question)
User: yes
User: let's talk about music(dialog act: command)
```

Another example dialog shown below can be formatted as : "do you want to tell me your name   : EMPTY : yes_no_question  & rose ##  statement;". In this case, there is only current user utterance so the previous utterance is input as EMPTY

```
System: do you want to tell me your name(dialog act: yes_no_question)
User: rose(dialog act: statement)
```

### Dialog act prediction

This part of the model is built on *"MIDAS, A Dialog Act Annotation Scheme for Open-Domain Human-Machine Spoken Conversations"*.

```
python run_classifier.py --data_dir da_data/ --bert_model bert-base-uncased --task_name da --output_dir output --do_eval --binary_pred  --do_train
```

- --data_dir: the data directory where training and evaluating data file is stored. 
- --bert_model: bert pre-trained model selected in the list: bert-base-uncased, bert-large-uncased, bert-base-cased, bert-large-cased, bert-base-multilingual-uncased, bert-base-multilingual-cased, bert-base-chinese
- --task_name: the name of the task to train, for example da represents dialog act
- --output_dir: the output directory where the model predictions and checkpoints will be written
- --do_eval: whether to evaluate on the evaluation file
- --do_train: whether to run training
- --binary_pred: whether to use Binary-Cross-Entropy for binary prediction instead of only one tag(in case of multi-dialog-act

## Semantic Role Labeling

The annotation follows the CONLL data format with only Semantic Role information.

The implementation of experiments mentioned in the paper above are based on AllenNLP framework [v0.8.4 master](https://github.com/allenai/allennlp/releases/tag/v0.8.4). The codes build AllenNLP
models and should be imported in an AllenNLP trainer.

Use the corresponding configuration files to run each experiment and include this paper_code as external package.

For example

```
allennlp evaluate ~/models/He2017_1600_2  ~/data/fine-tune/test --include-package paper_code
```

## Utterance Completion

We leverage OpenNMT for utterance completion. All the parameters are set to default ones: 2-layer LSTM with copy mechanism

The input and output format for utterance completion:

```
input: system utterance  &  user previous utterance # user current utterance(without completion)
output: user current utterance(with completion)
```

Specifically, the command line to train and evaluate a completion model:

```
python preprocess.py -train_src dialog-data/src-train.txt -train_tgt dialog-data/tgt-train.txt -save_data dialog-data/demo -dynamic_dict
```

```
python train.py -data dialog-data/demo -save_model demo-model -world_size 1 -gpu_ranks 0 -copy_attn
```

```
python translate.py -model demo-model_stepXXX.pt -src dialog-data/src-test.txt -output pred-test.txt -replace_unk -verbose

```

For any questions, please email: zhangxiyuan@zju.edu.cn.
