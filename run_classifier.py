# coding=utf-8
# Copyright 2018 The Google AI Language Team Authors and The HugginFace Inc. team.
# Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# The code is built on :cite:`yu2019midas`.
"""BERT finetuning runner."""

from __future__ import absolute_import, division, print_function

import argparse
import csv
import logging
import os
import random
import sys
import linecache

import numpy as np
import torch
from torch.utils.data import (DataLoader, RandomSampler, SequentialSampler,
                              TensorDataset)
from torch.utils.data.distributed import DistributedSampler
from tqdm import tqdm, trange

from pytorch_pretrained_bert.file_utils import PYTORCH_PRETRAINED_BERT_CACHE
from pytorch_pretrained_bert.modeling import BertForSequenceClassification, BertEllipsisCompletion,\
    BertConfig, WEIGHTS_NAME, CONFIG_NAME
from pytorch_pretrained_bert.tokenization import BertTokenizer
from pytorch_pretrained_bert.optimization import BertAdam, warmup_linear
from collections import defaultdict

error_dict = defaultdict(lambda: 0)

logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(name)s -   %(message)s',
                    datefmt = '%m/%d/%Y %H:%M:%S',
                    level = logging.INFO)
logger = logging.getLogger(__name__)


NOTE_DA_MAP = {"sd": "statement", "b": "back-channeling", "sv": "opinion", "ny": "pos_answer", "%": "abandon",
                       "ba": "appreciation",
                       "qy": "yes_no_question", "fc": "closing", "ng": "neg_answer", "h": "other_answers", "o": "other",
                       "ad": "command", "^h": "hold", "cp": "complaint", "fp": "opening",
                       "bd": "respond_to_apology",
                       "fa": "apology", "ft": "thanking",
                       "oqf": "open_question_factual", "oqo": "open_question_opinion", "cm": "comment",
                       "ns": "nonsense", "dad": "dev_command"
                       }

non_completion_list = ["back-channeling", "abandon", "appreciation", "closing", "other", "hold", "complaint",
                               "opening","respond_to_apology", "apology", "thanking", "nonsense", "dev_command"]

non_completion_list = ["hold","complaint","thanking"]#try each label one at a time
non_completion_id={}
for i, label in enumerate(list(NOTE_DA_MAP.values())):
    if label in non_completion_list:
        non_completion_id[label]=i

da_lookup = list(NOTE_DA_MAP.values())
maxmask_elli=torch.tensor([1 for i in range(len(NOTE_DA_MAP))]).cuda()
maxmask_comp=torch.tensor([0 if da_lookup[i] in non_completion_list else 1 for i in range(len(NOTE_DA_MAP))],dtype=torch.float).cuda()
addmask_elli=torch.tensor([2 if da_lookup[i] in non_completion_list else 1 for i in range(len(NOTE_DA_MAP))],dtype=torch.float).cuda()
addmask_comp=torch.tensor([0 if da_lookup[i] in non_completion_list else 1 for i in range(len(NOTE_DA_MAP))],dtype=torch.float).cuda()
dev_num = 1
pred_label=open('pred_label_ellielli.txt','w')
eval_list = []

class InputExample(object):
    """A single training/test example for simple sequence classification."""

    def __init__(self, guid, text_a, text_b=None, label=None):
        """Constructs a InputExample.

        Args:
            guid: Unique id for the example.
            text_a: string. The untokenized text of the first sequence. For single
            sequence tasks, only this sequence must be specified.
            text_b: (Optional) string. The untokenized text of the second sequence.
            Only must be specified for sequence pair tasks.
            label: (Optional) string. The label of the example. This should be
            specified for train and dev examples, but not for test examples.
        """
        self.guid = guid
        self.text_a = text_a
        self.text_b = text_b
        self.label = label


class InputFeatures(object):
    """A single set of features of data."""

    def __init__(self, input_ids, input_mask, segment_ids, label_id):
        self.input_ids = input_ids
        self.input_mask = input_mask
        self.segment_ids = segment_ids
        self.label_id = label_id


class DataProcessor(object):
    """Base class for data converters for sequence classification data sets."""

    def get_train_examples(self, data_dir):
        """Gets a collection of `InputExample`s for the train set."""
        raise NotImplementedError()

    def get_dev_examples(self, data_dir):
        """Gets a collection of `InputExample`s for the dev set."""
        raise NotImplementedError()

    def get_labels(self):
        """Gets the list of labels for this data set."""
        raise NotImplementedError()

    @classmethod
    def _read_tsv(cls, input_file, quotechar=None):
        """Reads a tab separated value file."""
        with open(input_file, "r") as f:
            reader = csv.reader(f, delimiter="\t", quotechar=quotechar)
            lines = []
            for line in reader:
                if sys.version_info[0] == 2:
                    line = list(unicode(cell, 'utf-8') for cell in line)
                lines.append(line)
            return lines


class MrpcProcessor(DataProcessor):
    """Processor for the MRPC data set (GLUE version)."""

    def get_train_examples(self, data_dir):
        """See base class."""
        logger.info("LOOKING AT {}".format(os.path.join(data_dir, "train.tsv")))
        return self._create_examples(
            self._read_tsv(os.path.join(data_dir, "train.tsv")), "train")

    def get_dev_examples(self, data_dir):
        """See base class."""
        return self._create_examples(
            self._read_tsv(os.path.join(data_dir, "dev.tsv")), "dev")

    def get_labels(self):
        """See base class."""
        return ["0", "1"]

    def _create_examples(self, lines, set_type):
        """Creates examples for the training and dev sets."""
        examples = []
        for (i, line) in enumerate(lines):
            if i == 0:
                continue
            guid = "%s-%s" % (set_type, i)
            text_a = line[3]
            text_b = line[4]
            label = line[0]
            examples.append(
                InputExample(guid=guid, text_a=text_a, text_b=text_b, label=label))
        return examples


class MnliProcessor(DataProcessor):
    """Processor for the MultiNLI data set (GLUE version)."""

    def get_train_examples(self, data_dir):
        """See base class."""
        return self._create_examples(
            self._read_tsv(os.path.join(data_dir, "train.tsv")), "train")

    def get_dev_examples(self, data_dir):
        """See base class."""
        return self._create_examples(
            self._read_tsv(os.path.join(data_dir, "dev_matched.tsv")),
            "dev_matched")

    def get_labels(self):
        """See base class."""
        return ["contradiction", "entailment", "neutral"]

    def _create_examples(self, lines, set_type):
        """Creates examples for the training and dev sets."""
        examples = []
        for (i, line) in enumerate(lines):
            if i == 0:
                continue
            guid = "%s-%s" % (set_type, line[0])
            text_a = line[8]
            text_b = line[9]
            label = line[-1]
            examples.append(
                InputExample(guid=guid, text_a=text_a, text_b=text_b, label=label))
        return examples


class DAProcessor(DataProcessor):
    """Processor for the dialog act data set"""

    def get_train_examples(self, data_dir, filename, binary_pred):
        """See base class."""
        return self._create_examples(
            os.path.join(data_dir, filename), "train", binary_pred)

    def get_dev_examples(self, data_dir, filename, binary_pred):
        """See base class."""
        return self._create_examples(
            os.path.join(data_dir, filename), "dev", binary_pred)

    def get_labels(self):
        """See base class."""
        # return ["oqf", "oqo", "qy", "ny", "ng", "h", "sd", "sv", "cm", "ad", "dad", "b",
        #     "ba", "%", "fp", "fc", "^h", "cp", "ft", "fa", "bd", "ns", "o", "bg", "qw"]

        NOTE_DA_MAP = {"sd": "statement", "b": "back-channeling", "sv": "opinion", "ny": "pos_answer", "%": "abandon",
                       "ba": "appreciation",
                       "qy": "yes_no_question", "fc": "closing", "ng": "neg_answer", "h": "other_answers", "o": "other",
                       "ad": "command", "^h": "hold", "cp": "complaint", "fp": "opening",
                       "bd": "respond_to_apology",
                       "fa": "apology", "ft": "thanking",
                       "oqf": "open_question_factual", "oqo": "open_question_opinion", "cm": "comment",
                       "ns": "nonsense", "dad": "dev_command"
                       }

        map_list = list(NOTE_DA_MAP.values())
        #map_list = ["S","D","B","F","Q"]
        #map_list = ["sd","b","sv","aa","% -", "ba",	"qy","x","ny","fc","%",	"qw","nn","bk",	"h","qy^d",
        #           "o","fo","bc","by","fw","bh","^q","bf",	"na","ny^e","ad","^2","b^m","qo","qh","^h","ar",
        #           "ng","nn^e","br","no","fp","qrr","arp","nd","t3","oo","cc","co","t1","bd","aap","am",
        #           "^g","qw^d","fa","ft"]

        return map_list

        # return ['sd', 'b', 'bk', 'sv', 'aa', '%', '% -', 'ba', 'qy', 'ny', 'fc', 'qw', 'nn', 'h', 'qy^d', 'o', 'fo', 'bc', 'by', 'fw', 'bh', '^q', 'bf', 'na', 'ny^e', 'ad', '^2', 'b^m', 'qo', 'qh', '^h', 'ar', 'ng', 'nn^e', 'br', 'no', 'fp', 'qrr', 'arp', 'nd', 'oo', 'cc', 'co', 't1', 'bd', 'aap', 'am', '^g', 'qw^d', 'fa', 'ft', 'oqf', 'oqo', 'cm', 'cp', 'ns', 'bg', 'dad']

    def _create_examples(self, filename, set_type, binary_pred):
        """Creates examples for the training and dev sets."""
        examples = []
        with open(filename) as fp:
            i=0
            for line in fp:
                # example line: "i think it usually does<:><emp> <rsp> does it say something<:>sd"
                guid = "%s-%s" % (set_type, i)
                i=i+1
                text = line.split("&")
                if(len(text)!=2):
                    continue
                text_a = text[0].strip()
                split_da = text[1].split("##")
                if(len(split_da)!=2):
                    continue
                text_b = split_da[0].strip()
                das = split_da[1].strip()
                label_1 = das.split(";")[0].strip()
                if binary_pred:
                    label_1 = das
                #ex:
                #text_a: how about another short piece of football news : EMPTY
                #text_b: how can you pick us knows now
                #label_1: open_question_factual;
                #guid: train-0
                examples.append(
                    InputExample(guid=guid, text_a=text_a, text_b=text_b, label=label_1))
                if set_type=="dev":
                    eval_list.append(line)

            return examples


class ColaProcessor(DataProcessor):
    """Processor for the CoLA data set (GLUE version)."""

    def get_train_examples(self, data_dir):
        """See base class."""
        return self._create_examples(
            self._read_tsv(os.path.join(data_dir, "train.tsv")), "train")

    def get_dev_examples(self, data_dir):
        """See base class."""
        return self._create_examples(
            self._read_tsv(os.path.join(data_dir, "dev.tsv")), "dev")

    def get_labels(self):
        """See base class."""
        return ["0", "1"]

    def _create_examples(self, lines, set_type):
        """Creates examples for the training and dev sets."""
        examples = []
        for (i, line) in enumerate(lines):
            guid = "%s-%s" % (set_type, i)
            text_a = line[3]
            label = line[1]
            examples.append(
                InputExample(guid=guid, text_a=text_a, text_b=None, label=label))
        return examples


def convert_examples_to_features(examples, label_list, max_seq_length, tokenizer, binary_pred):
    """Loads a data file into a list of `InputBatch`s."""

    label_map = {label : i for i, label in enumerate(label_list)}

    features = []
    for (ex_index, example) in enumerate(examples):
        tokens_a = tokenizer.tokenize(example.text_a)
        tokens_b = None
        if example.text_b:
            tokens_b = tokenizer.tokenize(example.text_b)
            # Modifies `tokens_a` and `tokens_b` in place so that the total
            # length is less than the specified length.
            # Account for [CLS], [SEP], [SEP] with "- 3"
            _truncate_seq_pair(tokens_a, tokens_b, max_seq_length - 3)
        else:
            # Account for [CLS] and [SEP] with "- 2"
            if len(tokens_a) > max_seq_length - 2:
                tokens_a = tokens_a[:(max_seq_length - 2)]

        # The convention in BERT is:
        # (a) For sequence pairs:
        #  tokens:   [CLS] is this jack ##son ##ville ? [SEP] no it is not . [SEP]
        #  type_ids: 0   0  0    0    0     0       0 0    1  1  1  1   1 1
        # (b) For single sequences:
        #  tokens:   [CLS] the dog is hairy . [SEP]
        #  type_ids: 0   0   0   0  0     0 0
        #
        # Where "type_ids" are used to indicate whether this is the first
        # sequence or the second sequence. The embedding vectors for `type=0` and
        # `type=1` were learned during pre-training and are added to the wordpiece
        # embedding vector (and position vector). This is not *strictly* necessary
        # since the [SEP] token unambigiously separates the sequences, but it makes
        # it easier for the model to learn the concept of sequences.
        #
        # For classification tasks, the first vector (corresponding to [CLS]) is
        # used as as the "sentence vector". Note that this only makes sense because
        # the entire model is fine-tuned.
        tokens = ["[CLS]"] + tokens_a + ["[SEP]"]
        segment_ids = [0] * len(tokens)

        if tokens_b:
            tokens += tokens_b + ["[SEP]"]
            segment_ids += [1] * (len(tokens_b) + 1)


        input_ids = tokenizer.convert_tokens_to_ids(tokens)

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        input_mask = [1] * len(input_ids)

        # Zero-pad up to the sequence length.
        padding = [0] * (max_seq_length - len(input_ids))
        input_ids += padding
        input_mask += padding
        segment_ids += padding


        assert len(input_ids) == max_seq_length
        assert len(input_mask) == max_seq_length
        assert len(segment_ids) == max_seq_length

        if binary_pred:
            label_1 = example.label.split(";")[0].strip()
            label_2 = example.label.split(";")[1].strip()
            label_id = [label_map[i] for i in (label_1, label_2) if len(i) > 0]
        else:
            label_id = label_map[example.label]



        if ex_index < 5:
            logger.info("*** Example ***")
            logger.info("guid: %s" % (example.guid))
            logger.info("tokens: %s" % " ".join(
                    [str(x) for x in tokens]))
            logger.info("input_ids: %s" % " ".join([str(x) for x in input_ids]))
            logger.info("input_mask: %s" % " ".join([str(x) for x in input_mask]))
            logger.info(
                    "segment_ids: %s" % " ".join([str(x) for x in segment_ids]))

            if isinstance(label_id, list):
                logger.info("label: %s (id = %s)" % (example.label, label_id))
            else:
                logger.info("label: %s (id = %d)" % (example.label, label_id))

        features.append(
                InputFeatures(input_ids=input_ids,
                              input_mask=input_mask,
                              segment_ids=segment_ids,
                              label_id=label_id))
    return features


def _truncate_seq_pair(tokens_a, tokens_b, max_length):
    """Truncates a sequence pair in place to the maximum length."""

    # This is a simple heuristic which will always truncate the longer sequence
    # one token at a time. This makes more sense than truncating an equal percent
    # of tokens from each, since if one sequence is very short then each token
    # that's truncated likely contains more information than a longer sequence.
    while True:
        total_length = len(tokens_a) + len(tokens_b)
        if total_length <= max_length:
            break
        if len(tokens_a) > len(tokens_b):
            tokens_a.pop()
        else:
            tokens_b.pop()

def accuracy(label_list, eval_list, eval_num, out, labels):
    outputs = np.argmax(out, axis=1)
    acc=0
    for i in range(len(outputs)):
        pred_label.write('%s-'%label_list[outputs[i]])
        pred_label.write('%s'%label_list[labels[i]])
        pred_label.write('%s'%eval_list[eval_num*8+i])
        if(outputs[i]==labels[i]):
            acc=acc+1
        else:
            pred_label.write('\n')
    #return np.sum(outputs == labels)
    return acc

def main():
    parser = argparse.ArgumentParser()

    ## Required parameters
    parser.add_argument("--data_dir",
                        default=None,
                        type=str,
                        required=True,
                        help="The input data dir. Should contain the .tsv files (or other data files) for the task.")
    parser.add_argument("--bert_model", default=None, type=str, required=True,
                        help="Bert pre-trained model selected in the list: bert-base-uncased, "
                        "bert-large-uncased, bert-base-cased, bert-large-cased, bert-base-multilingual-uncased, "
                        "bert-base-multilingual-cased, bert-base-chinese.")
    parser.add_argument("--task_name",
                        default=None,
                        type=str,
                        required=True,
                        help="The name of the task to train.")
    parser.add_argument("--output_dir",
                        default=None,
                        type=str,
                        required=True,
                        help="The output directory where the model predictions and checkpoints will be written.")

    ## Other parameters
    parser.add_argument("--cache_dir",
                        default="",
                        type=str,
                        help="Where do you want to store the pre-trained models downloaded from s3")
    parser.add_argument("--max_seq_length",
                        default=128,
                        type=int,
                        help="The maximum total input sequence length after WordPiece tokenization. \n"
                             "Sequences longer than this will be truncated, and sequences shorter \n"
                             "than this will be padded.")
    parser.add_argument("--do_train",
                        action='store_true',
                        help="Whether to run training.")
    parser.add_argument("--do_eval",
                        action='store_true',
                        help="Whether to run eval on the dev set.")
    parser.add_argument("--do_lower_case",
                        action='store_true',
                        help="Set this flag if you are using an uncased model.")
    parser.add_argument("--train_batch_size",
                        default=16,
                        type=int,
                        help="Total batch size for training.")
    parser.add_argument("--eval_batch_size",
                        default=8,
                        type=int,
                        help="Total batch size for eval.")
    parser.add_argument("--learning_rate",
                        default=5e-5,
                        type=float,
                        help="The initial learning rate for Adam.")
    parser.add_argument("--num_train_epochs",
                        default=2.0,
                        type=float,
                        help="Total number of training epochs to perform.")
    parser.add_argument("--warmup_proportion",
                        default=0.1,
                        type=float,
                        help="Proportion of training to perform linear learning rate warmup for. "
                             "E.g., 0.1 = 10%% of training.")
    parser.add_argument("--no_cuda",
                        action='store_true',
                        help="Whether not to use CUDA when available")
    parser.add_argument("--local_rank",
                        type=int,
                        default=-1,
                        help="local_rank for distributed training on gpus")
    parser.add_argument('--seed',
                        type=int,
                        default=42,
                        help="random seed for initialization")
    parser.add_argument('--gradient_accumulation_steps',
                        type=int,
                        default=1,
                        help="Number of updates steps to accumulate before performing a backward/update pass.")
    parser.add_argument('--fp16',
                        action='store_true',
                        help="Whether to use 16-bit float precision instead of 32-bit")
    parser.add_argument('--binary_pred',
                        action='store_true',
                        help="Whether to use BCE for binary prediction instead of only one tag")
    parser.add_argument('--add_expert',
                        action='store_true',
                        help="whether to add expert knowledge")
    parser.add_argument('--binary_threshold',
                        type=float,
                        default=0.5,
                        help="threshold for prediction")
    parser.add_argument('--loss_scale',
                        type=float, default=0,
                        help="Loss scaling to improve fp16 numeric stability. Only used when fp16 set to True.\n"
                             "0 (default value): dynamic loss scaling.\n"
                             "Positive power of 2: static loss scaling value.\n")
    parser.add_argument('--server_ip', type=str, default='', help="Can be used for distant debugging.")
    parser.add_argument('--server_port', type=str, default='', help="Can be used for distant debugging.")
    args = parser.parse_args()

    if args.server_ip and args.server_port:
        # Distant debugging - see https://code.visualstudio.com/docs/python/debugging#_attach-to-a-local-script
        import ptvsd
        print("Waiting for debugger attach")
        ptvsd.enable_attach(address=(args.server_ip, args.server_port), redirect_output=True)
        ptvsd.wait_for_attach()

    processors = {
        "cola": ColaProcessor,
        "mnli": MnliProcessor,
        "mrpc": MrpcProcessor,
        "da": DAProcessor,
    }

    num_labels_task = {
        "cola": 2,
        "mnli": 3,
        "mrpc": 2,
        "da": 23,
    }

    if args.local_rank == -1 or args.no_cuda:
        device = torch.device("cuda" if torch.cuda.is_available() and not args.no_cuda else "cpu")
        n_gpu = torch.cuda.device_count()
    else:
        torch.cuda.set_device(args.local_rank)
        device = torch.device("cuda", args.local_rank)
        n_gpu = 1
        # Initializes the distributed backend which will take care of sychronizing nodes/GPUs
        torch.distributed.init_process_group(backend='nccl')
    logger.info("device: {} n_gpu: {}, distributed training: {}, 16-bits training: {}".format(
        device, n_gpu, bool(args.local_rank != -1), args.fp16))

    if args.gradient_accumulation_steps < 1:
        raise ValueError("Invalid gradient_accumulation_steps parameter: {}, should be >= 1".format(
                            args.gradient_accumulation_steps))

    args.train_batch_size = args.train_batch_size // args.gradient_accumulation_steps

    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if n_gpu > 0:
        torch.cuda.manual_seed_all(args.seed)

    if not args.do_train and not args.do_eval:
        raise ValueError("At least one of `do_train` or `do_eval` must be True.")
    '''
    if os.path.exists(args.output_dir) and os.listdir(args.output_dir) and args.do_train:
        raise ValueError("Output directory ({}) already exists and is not empty.".format(args.output_dir))
    '''
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    task_name = args.task_name.lower()

    if task_name not in processors:
        raise ValueError("Task not found: %s" % (task_name))

    processor = processors[task_name]()
    label_list = processor.get_labels()
    #num_labels = num_labels_task[task_name]
    num_labels = len(label_list)

    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased", do_lower_case=args.do_lower_case)
    # tokenizer = BertTokenizer.from_pretrained(args.bert_model, do_lower_case=args.do_lower_case)

    train_examples_elli = None
    train_examples_comp = None
    num_train_optimization_steps = None
    if args.do_train:
        train_examples_elli = processor.get_train_examples(args.data_dir, "train-elli.txt", args.binary_pred)
        train_examples_comp = processor.get_train_examples(args.data_dir, "train-comp.txt", args.binary_pred)
        num_train_optimization_steps = int(
            (len(train_examples_elli)+len(train_examples_comp)) / args.train_batch_size / args.gradient_accumulation_steps) * args.num_train_epochs
        if args.local_rank != -1:
            num_train_optimization_steps = num_train_optimization_steps // torch.distributed.get_world_size()


    # Prepare model
    cache_dir = args.cache_dir if args.cache_dir else os.path.join(PYTORCH_PRETRAINED_BERT_CACHE, 'distributed_{}'.format(args.local_rank))
    bert1 = BertForSequenceClassification.from_pretrained(args.bert_model, cache_dir=cache_dir, num_labels=num_labels)
    bert2 = BertForSequenceClassification.from_pretrained(args.bert_model, cache_dir=cache_dir, num_labels=num_labels)
    model = BertEllipsisCompletion(bert1=bert1,bert2=bert2,num_labels=num_labels)


    if args.fp16:
        model.half()
    model.to(device)
    if args.local_rank != -1:
        try:
            from apex.parallel import DistributedDataParallel as DDP
        except ImportError:
            raise ImportError("Please install apex from https://www.github.com/nvidia/apex to use distributed and fp16 training.")

        model = DDP(model)
    elif n_gpu > 1:
        model = torch.nn.DataParallel(model)

    # Prepare optimizer
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay': 0.01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
        ]
    if args.fp16:
        try:
            from apex.optimizers import FP16_Optimizer
            from apex.optimizers import FusedAdam
        except ImportError:
            raise ImportError("Please install apex from https://www.github.com/nvidia/apex to use distributed and fp16 training.")

        optimizer = FusedAdam(optimizer_grouped_parameters,
                              lr=args.learning_rate,
                              bias_correction=False,
                              max_grad_norm=1.0)
        if args.loss_scale == 0:
            optimizer = FP16_Optimizer(optimizer, dynamic_loss_scale=True)
        else:
            optimizer = FP16_Optimizer(optimizer, static_loss_scale=args.loss_scale)

    else:
        optimizer = BertAdam(optimizer_grouped_parameters,
                             lr=args.learning_rate,
                             warmup=args.warmup_proportion,
                             t_total=num_train_optimization_steps)


    global_step = 0
    nb_tr_steps = 0
    tr_loss = 0
    if args.do_train:
        train_features_elli = convert_examples_to_features(
            train_examples_elli, label_list, args.max_seq_length, tokenizer, args.binary_pred)
        train_features_comp = convert_examples_to_features(
            train_examples_comp, label_list, args.max_seq_length, tokenizer, args.binary_pred)
        logger.info("***** Running training *****")
        logger.info("  Num examples = %d", len(train_examples_elli)+len(train_examples_comp))
        logger.info("  Batch size = %d", args.train_batch_size)
        logger.info("  Num steps = %d", num_train_optimization_steps)
        all_input_ids_elli = torch.tensor([f.input_ids for f in train_features_elli], dtype=torch.long)
        all_input_mask_elli = torch.tensor([f.input_mask for f in train_features_elli], dtype=torch.long)
        all_segment_ids_elli = torch.tensor([f.segment_ids for f in train_features_elli], dtype=torch.long)
        all_input_ids_comp = torch.tensor([f.input_ids for f in train_features_comp], dtype=torch.long)
        all_input_mask_comp = torch.tensor([f.input_mask for f in train_features_comp], dtype=torch.long)
        all_segment_ids_comp = torch.tensor([f.segment_ids for f in train_features_comp], dtype=torch.long)

        # label_id is a list of ids (len = 1 or 2)
        # use float instead of long for BCE loss
        if args.binary_pred:
            #to one hot encoding
            def prepare_binary_tag(label_ids):
                x = [0] * num_labels
                for l_id in label_ids:
                    x[l_id] = 1
                # return torch.tensor(x, dtype=torch.float, device=device).view(-1, num_labels)
                return x

            all_label_ids_elli = torch.tensor([prepare_binary_tag(f.label_id) for f in train_features_elli], dtype=torch.float)
            all_label_ids_comp = torch.tensor([prepare_binary_tag(f.label_id) for f in train_features_comp], dtype=torch.float)
        else:
            all_label_ids_elli = torch.tensor([f.label_id for f in train_features_elli], dtype=torch.long)
            all_label_ids_comp = torch.tensor([f.label_id for f in train_features_comp], dtype=torch.long)

        train_data = TensorDataset(all_input_ids_elli, all_input_mask_elli, all_segment_ids_elli, all_label_ids_elli,
                                   all_input_ids_comp, all_input_mask_comp, all_segment_ids_comp, all_label_ids_comp)
        if args.local_rank == -1:
            train_sampler = RandomSampler(train_data)
        else:
            train_sampler = DistributedSampler(train_data)
        train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=args.train_batch_size)

        model.train()
        binary_pred = args.binary_pred
        for _ in trange(int(args.num_train_epochs), desc="Epoch"):
            tr_loss = 0
            nb_tr_examples, nb_tr_steps = 0, 0
            for step, batch in enumerate(tqdm(train_dataloader, desc="Iteration")):
                batch = tuple(t.to(device) for t in batch)
                input_ids_elli, input_mask_elli, segment_ids_elli, label_ids_elli, \
                input_ids_comp, input_mask_comp, segment_ids_comp, label_ids_comp = batch
                #print('prepare batch')
                loss = model(input_ids_elli, segment_ids_elli, input_mask_elli, label_ids_elli, addmask_elli.expand(input_ids_elli.size(0),-1),
                             input_ids_comp, segment_ids_comp, input_mask_comp, label_ids_comp, addmask_comp.expand(input_ids_elli.size(0),-1), binary_pred)
                #print('loss=',loss)
                if n_gpu > 1:
                    loss = loss.mean() # mean() to average on multi-gpu.
                if args.gradient_accumulation_steps > 1:
                    loss = loss / args.gradient_accumulation_steps

                if args.fp16:
                    optimizer.backward(loss)
                else:
                    loss.backward()
                #print('backward')

                tr_loss += loss.item()
                #nb_tr_examples += input_ids.size(0)
                nb_tr_steps += 1
                if (step + 1) % args.gradient_accumulation_steps == 0:
                    if args.fp16:
                        # modify learning rate with special warm up BERT uses
                        # if args.fp16 is False, BertAdam is used that handles this automatically
                        lr_this_step = args.learning_rate * warmup_linear(global_step/num_train_optimization_steps, args.warmup_proportion)
                        for param_group in optimizer.param_groups:
                            param_group['lr'] = lr_this_step
                    optimizer.step()
                    optimizer.zero_grad()
                    global_step += 1
                #print('optimization')

    output_model_file = os.path.join(args.output_dir, "ellicomp-2epochs_add_hidden.bin")
    if args.do_train:
        '''
        # Save a trained model and the associated configuration
        model_to_save = model.module if hasattr(model, 'module') else model  # Only save the model it-self
        torch.save(model_to_save.state_dict(), output_model_file)
        output_config_file = os.path.join(args.output_dir, CONFIG_NAME)
        with open(output_config_file, 'w') as f:
            f.write(model_to_save.config.to_json_string())

        # Load a trained model and config that you have fine-tuned
        config = BertConfig(output_config_file)
        bert1 = BertForSequenceClassification(config, num_labels=num_labels)
        bert2 = BertForSequenceClassification(config, num_labels=num_labels)
        model = BertEllipsisCompletion(bert1=bert1, bert2=bert2, num_labels=num_labels)
        model.load_state_dict(torch.load(output_model_file))
        '''
        torch.save(model.state_dict(),output_model_file)
        model.load_state_dict(torch.load(output_model_file))
    if not args.do_train:
        #model.load_state_dict(torch.load(output_model_file))
        '''
        bert1 = BertForSequenceClassification.from_pretrained(args.bert_model, cache_dir=cache_dir, num_labels=num_labels)
        bert2 = BertForSequenceClassification.from_pretrained(args.bert_model, cache_dir=cache_dir, num_labels=num_labels)
        model = BertEllipsisCompletion(bert1=bert1, bert2=bert2, num_labels=num_labels)
        '''
        model.load_state_dict(torch.load(output_model_file))
    model.to(device)

    if args.do_eval and (args.local_rank == -1 or torch.distributed.get_rank() == 0):
        eval_examples_elli = processor.get_dev_examples(args.data_dir, "eval-elli.txt", args.binary_pred)
        eval_examples_comp = processor.get_dev_examples(args.data_dir, "eval-comp.txt", args.binary_pred)
        eval_features_elli = convert_examples_to_features(
            eval_examples_elli, label_list, args.max_seq_length, tokenizer, args.binary_pred)
        eval_features_comp = convert_examples_to_features(
            eval_examples_comp, label_list, args.max_seq_length, tokenizer, args.binary_pred)
        logger.info("***** Running evaluation *****")
        logger.info("  Num examples = %d", len(eval_examples_elli)+len(eval_features_comp))
        logger.info("  Batch size = %d", args.eval_batch_size)
        all_input_ids_elli = torch.tensor([f.input_ids for f in eval_features_elli], dtype=torch.long)
        all_input_mask_elli = torch.tensor([f.input_mask for f in eval_features_elli], dtype=torch.long)
        all_segment_ids_elli = torch.tensor([f.segment_ids for f in eval_features_elli], dtype=torch.long)
        all_input_ids_comp = torch.tensor([f.input_ids for f in eval_features_comp], dtype=torch.long)
        all_input_mask_comp = torch.tensor([f.input_mask for f in eval_features_comp], dtype=torch.long)
        all_segment_ids_comp = torch.tensor([f.segment_ids for f in eval_features_comp], dtype=torch.long)

        if args.binary_pred:
            def prepare_binary_tag(label_ids):
                x = [0] * num_labels
                for l_id in label_ids:
                    x[l_id] = 1
                return x

            all_label_ids_elli = torch.tensor([prepare_binary_tag(f.label_id) for f in eval_features_elli], dtype=torch.float)
            all_label_ids_comp = torch.tensor([prepare_binary_tag(f.label_id) for f in eval_features_comp], dtype=torch.float)
        else:
            all_label_ids_elli = torch.tensor([f.label_id for f in eval_features_elli], dtype=torch.long)
            all_label_ids_comp = torch.tensor([f.label_id for f in eval_features_comp], dtype=torch.long)
            print(len(all_input_ids_elli))

        eval_data = TensorDataset(all_input_ids_elli, all_input_mask_elli, all_segment_ids_elli, all_label_ids_elli,
                                   all_input_ids_comp, all_input_mask_comp, all_segment_ids_comp, all_label_ids_comp)
        # Run prediction for full data
        eval_sampler = SequentialSampler(eval_data)
        eval_dataloader = DataLoader(eval_data, sampler=eval_sampler, batch_size=args.eval_batch_size)

        model.eval()
        eval_loss, eval_accuracy = 0, 0
        nb_eval_steps, nb_eval_examples = 0, 0
        total_p, total_r, total_f1 = 0, 0, 0

        dev_num_1 = 1
        eval_num=0

        for input_ids_elli, input_mask_elli, segment_ids_elli, label_ids_elli, \
            input_ids_comp, input_mask_comp, segment_ids_comp, label_ids_comp in tqdm(eval_dataloader, desc="Evaluating"):
            input_ids_elli = input_ids_elli.to(device)
            input_mask_elli = input_mask_elli.to(device)
            segment_ids_elli = segment_ids_elli.to(device)
            label_ids_elli = label_ids_elli.to(device)
            input_ids_comp = input_ids_comp.to(device)
            input_mask_comp = input_mask_comp.to(device)
            segment_ids_comp = segment_ids_comp.to(device)
            label_ids_comp = label_ids_comp.to(device)

            with torch.no_grad():
                tmp_eval_loss = model(input_ids_elli, segment_ids_elli, input_mask_elli, label_ids_elli, addmask_elli.expand(input_ids_elli.size(0),-1),
                                      input_ids_comp, segment_ids_comp, input_mask_comp, label_ids_comp, addmask_comp.expand(input_ids_elli.size(0),-1), args.binary_pred)
                logits,logits1 = model(input_ids_elli, segment_ids_elli, input_mask_elli, None, addmask_elli.expand(input_ids_elli.size(0),-1),
                               input_ids_comp, segment_ids_comp, input_mask_comp, None, addmask_comp.expand(input_ids_elli.size(0),-1), args.binary_pred)

            # print("size")
            # print(logits.size())
            # print(label_ids.size())
            # print(label_ids)
            # print()

            if args.binary_pred:



                for tgt_label, pred_da, pred_da1 in zip(label_ids_elli, logits, logits1):
                    tgt_ids = []

                    for i in np.nonzero(tgt_label).view(-1).data.cpu().numpy():
                        tgt_ids.append(i)

                    # first evaluate using ellipsis prediction logits
                    top_k_value, top_k_ind = torch.topk(pred_da1, 2)
                    top_id_data = []
                    for k_value, k_ind in zip(torch.sigmoid(top_k_value).view(-1).data.cpu().numpy(),
                                              top_k_ind.view(-1).data.cpu().numpy()):
                        if k_value > args.binary_threshold:
                            top_id_data.append(k_ind)
                    if len(top_id_data) == 0:
                        top_id_data.append(top_k_ind.view(-1).data.cpu().numpy()[0])

                    # print(dev_num_1)
                    # dev_num_1 += 1

                    elliflag=0
                    for pred in top_id_data:
                        if pred in non_completion_id.values():
                            elliflag=1


                    if elliflag and args.add_expert:
                        print('elli')
                        dev_num_1, p, r, f1 = get_F1_score(eval_list[eval_num], top_id_data, tgt_ids, dev_num_1)
                    else:
                        #not in non-completion-list, so we use ellipsis+complete
                        top_k_value, top_k_ind = torch.topk(pred_da, 2)
                        top_id_data = []
                        for k_value, k_ind in zip(torch.sigmoid(top_k_value).view(-1).data.cpu().numpy(),
                                                  top_k_ind.view(-1).data.cpu().numpy()):
                            if k_value > args.binary_threshold:
                                top_id_data.append(k_ind)
                        if len(top_id_data) == 0:
                            top_id_data.append(top_k_ind.view(-1).data.cpu().numpy()[0])
                        dev_num_1, p, r, f1 = get_F1_score(eval_list[eval_num], top_id_data, tgt_ids, dev_num_1)


                    total_p += p
                    total_r += r
                    total_f1 += f1
                    eval_num += 1


            else:
                logits = logits.detach().cpu().numpy()
                label_ids_elli = label_ids_elli.to(device)
                tmp_eval_accuracy = accuracy(label_list, eval_list, eval_num, logits, label_ids_elli)

                eval_loss += tmp_eval_loss.mean().item()
                eval_accuracy += tmp_eval_accuracy
                eval_num +=1

            nb_eval_examples += input_ids_elli.size(0)
            print(nb_eval_examples)
            nb_eval_steps += 1

        eval_loss = eval_loss / nb_eval_steps

        if args.binary_pred:
            eval_accuracy = "p, r, f1: %.4f, %.4f, %.4f" % (total_p / nb_eval_examples,
                                                            total_r / nb_eval_examples, total_f1 / nb_eval_examples)
        else:
            eval_accuracy = eval_accuracy / nb_eval_examples
        loss = tr_loss/nb_tr_steps if args.do_train else None
        result = {'eval_loss': eval_loss,
                  'eval_accuracy': eval_accuracy,
                  'global_step': global_step,
                  'loss': loss}

        output_eval_file = os.path.join(args.output_dir, "eval_results.txt")
        with open(output_eval_file, "w") as writer:
            logger.info("***** Eval results *****")
            for key in sorted(result.keys()):
                logger.info("  %s = %s", key, str(result[key]))
                writer.write("%s = %s\n" % (key, str(result[key])))

        # for error_da in sorted(error_dict.items(), key=lambda kv: kv[1], reverse=True):
        #     print(error_da)
        print(sorted(error_dict.items(), key=lambda kv: kv[1], reverse=True))


def get_F1_score(cur, pred_das, tgt_das, dev_num):
    true_pos = 0

    for pred in pred_das:
        if pred in tgt_das:
            true_pos += 1

    p = true_pos / len(pred_das)
    if len(tgt_das)!=0:
        r = true_pos / len(tgt_das)
    else:
        r=0
    if p == r == 0:
        f1 = 0
    else:
        f1 = 2 * p * r / (p + r)


    pred_das_c = []
    for i in pred_das:
        pred_das_c.append(da_lookup[int(i)])
    tgt_das_c = []
    for i in tgt_das:
        tgt_das_c.append(da_lookup[int(i)])

    pred_das_c.sort()
    tgt_das_c.sort()


    # if pred_das_c.sort() != tgt_das_c.sort():
    # print(dev_num)
    # dev_num += 1
    # print(pred_das_c)
    # print(tgt_das_c)
    # print()
    #pred_label.write('%s\n' % " ".join(pred_das_c))

    pred_label.write('%s-' % "&".join(pred_das_c))
    pred_label.write('%s;' % "&".join(tgt_das_c))
    pred_label.write('%s' % cur)

    if pred_das_c != tgt_das_c:
        error = "&".join(pred_das_c) + " - " + "&".join(tgt_das_c)
        error_dict[error] += 1
        pred_label.write('\n')
        '''
        pred_label.write('%s-' % "&".join(pred_das_c))
        pred_label.write('%s;' % "&".join(tgt_das_c))
        pred_label.write('%s\n' % cur)
        '''
        if error == "opinion - abandon":
            print(dev_num)

    return dev_num+1, p, r, f1


if __name__ == "__main__":
    main()
